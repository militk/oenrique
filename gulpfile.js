"use strict";

const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const browserSync = require('browser-sync').create();
const plumber = require('gulp-plumber');
const compileHandlebars = require('gulp-compile-handlebars');
const trim = require('gulp-trim');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const spritesmith  = require('gulp.spritesmith');
const babel = require('gulp-babel');

gulp.task('clean',
    del.bind(null, ['./.tmp'], {dot: true})
);

gulp.task('serve', function () {
  browserSync.init({
    notify: false,
    logPrefix: 'WSK',
    logFileChanges: false,
    server: ['./.tmp'],
    startPath: '/html/',
    logSnippet: false
  });
});

gulp.task('sass', function () {
  return gulp.src('./app/sass/main.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: 1
      }))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./.tmp/css'))
      .pipe(browserSync.stream());
});

gulp.task('html', function () {
  const data = {
        j_title: ''
      },
      options = {
        ignorePartials: true,
        batch: [
          './app/html/layouts',
          './app/html/partials'
        ],
        helpers: {
          times: function (n, block) {
            var accum = '';
            for (var i = 0; i < n; ++i)
              accum += block.fn(i + 1);
            return accum;
          },
          ifCond: function (v1, v2, options) {
            if (v1 === v2) {
              return options.fn(this);
            }
            return options.inverse(this);
          }
        }
      };

  return gulp.src([
    './app/html/**/*.hbs',
    '!./app/html/layouts/**/*.hbs',
    '!./app/html/partials/**/*.hbs'
  ])
      .pipe(plumber())
      .pipe(compileHandlebars(data, options))
      .pipe(rename(path => {
        path.extname = ".html"
      }))
      .pipe(trim())
      .pipe(gulp.dest('./.tmp/html'))
      .pipe(browserSync.stream());
});

gulp.task('sprite', function() {
    let spriteData =
        gulp.src('./app/img/sprite/*.*')
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.scss',
                cssFormat: 'scss',
                cssTemplate: 'config/scss.template.mustache',
                cssVarMap: function(sprite) {
                    sprite.name = 's-' + sprite.name
                }
            }));

    return (spriteData.img.pipe(gulp.dest('./.tmp/img/sprite')),
    spriteData.css.pipe(gulp.dest('./app/sass/libs/sprite')));

});

gulp.task('js', () =>
     gulp.src('./app/js/**/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('./.tmp/js'))
);

gulp.task('content', function () {
   return (gulp.src('./app/fonts/**/*').pipe(gulp.dest('./.tmp/fonts')),
    gulp.src('./app/img/*').pipe(gulp.dest('./.tmp/img')))
});

gulp.task('public', function () {
  return gulp.src('./.tmp/**/*').pipe(gulp.dest('./public'));
});

gulp.task('watch', function () {
  gulp.watch(['./app/sass/**/*'], gulp.series('sass'));
  gulp.watch(['./app/html/**/*'], gulp.series('html'));
  gulp.watch(['./app/img/sprite/*'], gulp.series('sprite'));
  gulp.watch(['./app/js/**/*'], gulp.series('js'));
});

gulp.task('dev', gulp.series(
    gulp.parallel('clean'),
    gulp.parallel('sprite', 'sass', 'html', 'content', 'js'),
    gulp.parallel('watch', 'serve')
));

gulp.task('build', gulp.series(
    gulp.parallel('clean'),
    gulp.parallel('sprite', 'sass', 'html', 'content', 'js'),
    gulp.parallel('public')
));