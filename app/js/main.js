"use strict";
const hamburger = document.querySelector('.header__hamburger');
const menu = document.querySelector('.header__menu');
const body = document.querySelector('body');
const menuBg = document.createElement('div');
menuBg.className = 'menu__bg-mobile';

hamburger.addEventListener("click", function () {
    menu.classList.toggle("open");
    body.appendChild(menuBg);
    body.style.cssText="overflow: hidden;";
});

menuBg.addEventListener("click", function () {
    menu.classList.remove("open");
    body.removeChild(menuBg);
    body.style.cssText="overflow:;";
});

$('.testimonials__owl').owlCarousel({
    loop:true,
    dots: true,
    items: 1,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true
});